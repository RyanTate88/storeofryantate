# README #

#Goals#

To create a database project that has four tables and four forms. Each table must have at least two queries that display results from the table, at
least two parameterized queries, and at least two queries that uses Math functions.
 
#Implementation#

First, I came up with a database schema for a basic store. I then created a database based on the schema in Visual Studio using Microsoft SQL Server.
Next, I added records to the tables. Then, I created queries for each table in the dataset. Then, I created the main form and work on its connections
opening all the other forms. Once the connections were made, I worked on each individual form, adding the table and buttons. Once the buttons were
added, I gave them functionality and married them to their corresponding query. When I completed a form, I made sure the form was working before I
moved on to the next form. Finally, after all the forms were done, I then went through the whole program and did extensive testing to ensure complete
functionality.
 
#How to use it#

To use this project, you will need at least Visual Studio 2015. You start off at the main form and can select any button to open its form. When on any
of the forms that you selected, you can view its entire table or push a button to perform a query. For search queries, you will need to provide the
proper input to proceed.