﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreOfRyanTate
{
    public partial class OrdersForm : Form
    {
        public OrdersForm()
        {
            InitializeComponent();
        }

        private void orderBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.orderBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.storeOfRyanTateDataSet);

        }

        private void OrdersForm_Load(object sender, EventArgs e)
        {
            
            // TODO: This line of code loads data into the 'storeOfRyanTateDataSet.Order' table. You can move, or remove it, as needed.
            this.orderTableAdapter.Fill(this.storeOfRyanTateDataSet.Order);

        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ordersOn3and5Button_Click(object sender, EventArgs e)
        {
            this.orderTableAdapter.FillByOrdersOn3OR5(this.storeOfRyanTateDataSet.Order);
        }

        private void orderTotalsButton_Click(object sender, EventArgs e)
        {
            OrderTotalsForm orderTotals = new OrderTotalsForm();

            orderTotals.ShowDialog();
        }
    }
}
