﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreOfRyanTate
{
    public partial class ProductsForm : Form
    {
        public ProductsForm()
        {
            InitializeComponent();
        }

        private void productsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.productsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.storeOfRyanTateDataSet);

        }

        private void ProductsForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'storeOfRyanTateDataSet.Products' table. You can move, or remove it, as needed.
            this.productsTableAdapter.FillBy(this.storeOfRyanTateDataSet.Products);

        }

        private void fiveAndUnderProductsButton_Click(object sender, EventArgs e)
        {
            this.productsTableAdapter.FillBy5DollarsOrLess(this.storeOfRyanTateDataSet.Products);
        }

        private void grossSalesPerProductButton_Click(object sender, EventArgs e)
        {
            ProductsTotalGrossForm totalGross = new ProductsTotalGrossForm();

            totalGross.ShowDialog();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchByNameButton_Click(object sender, EventArgs e)
        {
            // declare variable
            string productName = "";
            int checkForValues = 0;

            if (productNameTextBox.Text != "")
            {
                productName = productNameTextBox.Text;

                checkForValues = this.productsTableAdapter.FillByProductNameSearch(this.storeOfRyanTateDataSet.Products, productName);
                if (checkForValues == 0)
                {
                    MessageBox.Show("No produt was found named: " + productName.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please enter a product name before searching");
                productNameTextBox.Focus();
            }
        }
    }
}
