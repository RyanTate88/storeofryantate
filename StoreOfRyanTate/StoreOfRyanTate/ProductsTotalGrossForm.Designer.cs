﻿namespace StoreOfRyanTate
{
    partial class ProductsTotalGrossForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductsTotalGrossForm));
            this.storeOfRyanTateDataSet = new StoreOfRyanTate.StoreOfRyanTateDataSet();
            this.products_Total_GrossBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.products_Total_GrossTableAdapter = new StoreOfRyanTate.StoreOfRyanTateDataSetTableAdapters.Products_Total_GrossTableAdapter();
            this.tableAdapterManager = new StoreOfRyanTate.StoreOfRyanTateDataSetTableAdapters.TableAdapterManager();
            this.products_Total_GrossBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.products_Total_GrossBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.products_Total_GrossDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.closeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.storeOfRyanTateDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.products_Total_GrossBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.products_Total_GrossBindingNavigator)).BeginInit();
            this.products_Total_GrossBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.products_Total_GrossDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // storeOfRyanTateDataSet
            // 
            this.storeOfRyanTateDataSet.DataSetName = "StoreOfRyanTateDataSet";
            this.storeOfRyanTateDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // products_Total_GrossBindingSource
            // 
            this.products_Total_GrossBindingSource.DataMember = "Products_Total_Gross";
            this.products_Total_GrossBindingSource.DataSource = this.storeOfRyanTateDataSet;
            // 
            // products_Total_GrossTableAdapter
            // 
            this.products_Total_GrossTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CustomerTableAdapter = null;
            this.tableAdapterManager.EmployeeTableAdapter = null;
            this.tableAdapterManager.OrderDetailsTableAdapter = null;
            this.tableAdapterManager.Products_Total_GrossTableAdapter = this.products_Total_GrossTableAdapter;
            this.tableAdapterManager.ProductsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = StoreOfRyanTate.StoreOfRyanTateDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // products_Total_GrossBindingNavigator
            // 
            this.products_Total_GrossBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.products_Total_GrossBindingNavigator.BindingSource = this.products_Total_GrossBindingSource;
            this.products_Total_GrossBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.products_Total_GrossBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.products_Total_GrossBindingNavigator.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.products_Total_GrossBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.products_Total_GrossBindingNavigatorSaveItem});
            this.products_Total_GrossBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.products_Total_GrossBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.products_Total_GrossBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.products_Total_GrossBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.products_Total_GrossBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.products_Total_GrossBindingNavigator.Name = "products_Total_GrossBindingNavigator";
            this.products_Total_GrossBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.products_Total_GrossBindingNavigator.Size = new System.Drawing.Size(1241, 31);
            this.products_Total_GrossBindingNavigator.TabIndex = 0;
            this.products_Total_GrossBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(28, 28);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(54, 28);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(28, 28);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(28, 28);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(28, 28);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 31);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 31);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(28, 28);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(28, 28);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // products_Total_GrossBindingNavigatorSaveItem
            // 
            this.products_Total_GrossBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.products_Total_GrossBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("products_Total_GrossBindingNavigatorSaveItem.Image")));
            this.products_Total_GrossBindingNavigatorSaveItem.Name = "products_Total_GrossBindingNavigatorSaveItem";
            this.products_Total_GrossBindingNavigatorSaveItem.Size = new System.Drawing.Size(28, 28);
            this.products_Total_GrossBindingNavigatorSaveItem.Text = "Save Data";
            this.products_Total_GrossBindingNavigatorSaveItem.Click += new System.EventHandler(this.products_Total_GrossBindingNavigatorSaveItem_Click);
            // 
            // products_Total_GrossDataGridView
            // 
            this.products_Total_GrossDataGridView.AutoGenerateColumns = false;
            this.products_Total_GrossDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.products_Total_GrossDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.products_Total_GrossDataGridView.DataSource = this.products_Total_GrossBindingSource;
            this.products_Total_GrossDataGridView.Location = new System.Drawing.Point(21, 54);
            this.products_Total_GrossDataGridView.Name = "products_Total_GrossDataGridView";
            this.products_Total_GrossDataGridView.RowTemplate.Height = 28;
            this.products_Total_GrossDataGridView.Size = new System.Drawing.Size(941, 782);
            this.products_Total_GrossDataGridView.TabIndex = 1;
            this.products_Total_GrossDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.products_Total_GrossDataGridView_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ProductID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ProductID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Price";
            this.dataGridViewTextBoxColumn3.HeaderText = "Price";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Quantity";
            this.dataGridViewTextBoxColumn4.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Gross_Total";
            this.dataGridViewTextBoxColumn5.HeaderText = "Gross_Total";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // closeButton
            // 
            this.closeButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.closeButton.FlatAppearance.BorderSize = 5;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.Location = new System.Drawing.Point(1037, 165);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(140, 91);
            this.closeButton.TabIndex = 2;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // ProductsTotalGrossForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 1011);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.products_Total_GrossDataGridView);
            this.Controls.Add(this.products_Total_GrossBindingNavigator);
            this.Name = "ProductsTotalGrossForm";
            this.Text = "ProductsTotalGrossForm";
            this.Load += new System.EventHandler(this.ProductsTotalGrossForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.storeOfRyanTateDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.products_Total_GrossBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.products_Total_GrossBindingNavigator)).EndInit();
            this.products_Total_GrossBindingNavigator.ResumeLayout(false);
            this.products_Total_GrossBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.products_Total_GrossDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private StoreOfRyanTateDataSet storeOfRyanTateDataSet;
        private System.Windows.Forms.BindingSource products_Total_GrossBindingSource;
        private StoreOfRyanTateDataSetTableAdapters.Products_Total_GrossTableAdapter products_Total_GrossTableAdapter;
        private StoreOfRyanTateDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator products_Total_GrossBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton products_Total_GrossBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView products_Total_GrossDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Button closeButton;
    }
}