﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreOfRyanTate
{
    public partial class CustomersForm : Form
    {
        public CustomersForm()
        {
            InitializeComponent();
        }

        private void customerBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.customerBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.storeOfRyanTateDataSet);

        }

        private void CustomersForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'storeOfRyanTateDataSet.Customer' table. You can move, or remove it, as needed.
            this.customerTableAdapter.Fill(this.storeOfRyanTateDataSet.Customer);

        }

        private void westRegionButton_Click(object sender, EventArgs e)
        {
            this.customerTableAdapter.FillByWesternRegion(this.storeOfRyanTateDataSet.Customer);
        }

        private void julyBDayButton_Click(object sender, EventArgs e)
        {
            this.customerTableAdapter.FillByJulyBirthdays(this.storeOfRyanTateDataSet.Customer);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchByStateButton_Click(object sender, EventArgs e)
        {
            // declare variable
            string state = "";
            int checkForValues = 0;

            if (stateTextBox.Text != "")
            {
                state = stateTextBox.Text;

                checkForValues = this.customerTableAdapter.FillByStateSearch(this.storeOfRyanTateDataSet.Customer, state);
                if (checkForValues == 0)
                {
                    MessageBox.Show("No state was found called: " + state.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please enter a state before searching");
                stateTextBox.Focus();
            }
        }
    }
}
