﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreOfRyanTate
{
    public partial class OrderTotalsForm : Form
    {
        public OrderTotalsForm()
        {
            InitializeComponent();
        }

        private void OrderTotalsForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'storeOfRyanTateDataSet.Order_Totals' table. You can move, or remove it, as needed.
            this.order_TotalsTableAdapter.Fill(this.storeOfRyanTateDataSet.Order_Totals);

        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
