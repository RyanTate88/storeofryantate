﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreOfRyanTate
{
    public partial class EmployeesForm : Form
    {
        public EmployeesForm()
        {
            InitializeComponent();
        }

        private void employeeBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.employeeBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.storeOfRyanTateDataSet);

        }

        private void employeeBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.employeeBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.storeOfRyanTateDataSet);

        }

        private void EmployeesForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'storeOfRyanTateDataSet.Employee' table. You can move, or remove it, as needed.
            this.employeeTableAdapter.Fill(this.storeOfRyanTateDataSet.Employee);

        }

        private void sortByCityButton_Click(object sender, EventArgs e)
        {
            this.employeeTableAdapter.FillByCity(this.storeOfRyanTateDataSet.Employee);
        }

        private void sortByPayOver11Button_Click(object sender, EventArgs e)
        {
            this.employeeTableAdapter.FillByPayRateAbove11(this.storeOfRyanTateDataSet.Employee);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchByCitybutton_Click(object sender, EventArgs e)
        {
            // declare variable
            string employeeName = "";
            int checkForValues = 0;

            if (employeeNameTextBox.Text != "")
            {
                employeeName = employeeNameTextBox.Text;

                checkForValues = this.employeeTableAdapter.FillByNameSearch(this.storeOfRyanTateDataSet.Employee, employeeName);
                if(checkForValues == 0)
                {
                    MessageBox.Show("No employee was found named: " + employeeName.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please enter an employees name before searching");
            }
        }
    }
}
