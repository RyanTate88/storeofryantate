﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreOfRyanTate
{
    public partial class ProductsTotalGrossForm : Form
    {
        public ProductsTotalGrossForm()
        {
            InitializeComponent();
        }

        private void products_Total_GrossBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.products_Total_GrossBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.storeOfRyanTateDataSet);

        }

        private void ProductsTotalGrossForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'storeOfRyanTateDataSet.Products_Total_Gross' table. You can move, or remove it, as needed.
            this.products_Total_GrossTableAdapter.Fill(this.storeOfRyanTateDataSet.Products_Total_Gross);

        }

        private void products_Total_GrossDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
