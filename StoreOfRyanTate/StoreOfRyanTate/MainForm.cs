﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreOfRyanTate
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void employeeButton_Click(object sender, EventArgs e)
        {
            EmployeesForm showEmployees = new EmployeesForm();

            showEmployees.ShowDialog();
        }

        private void customersButton_Click(object sender, EventArgs e)
        {
            CustomersForm showCustomers = new CustomersForm();

            showCustomers.ShowDialog();
        }

        private void productsButton_Click(object sender, EventArgs e)
        {
            ProductsForm showProducts = new ProductsForm();

            showProducts.ShowDialog();
        }

        private void ordersButton_Click(object sender, EventArgs e)
        {
            OrdersForm showOrders = new OrdersForm();

            showOrders.ShowDialog();
        }
    }
}
